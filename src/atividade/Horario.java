package atividade;

public class Horario {

	private byte hora; // {0 .. 23}
	private byte minuto; // {0 .. 59}
	private byte segundo; // {0 .. 59}

	public Horario() {
		setHora((byte) 0);
		setMinuto((byte) 0);
		setSegundo((byte) 0);
	}

	public Horario(byte hora, byte minuto, byte segundo) {
		setHora(hora);
		setMinuto(minuto);
		setSegundo(segundo);
	}

	public Horario(int hora, int minuto, int segundo) {
		this((byte) hora, (byte) minuto, (byte) segundo);
	}

	public Horario(Horario horario) {
		this(horario.getHora(), horario.getMinuto(), horario.getSegundo());
	}

	public void setHora(byte hora) {

		if (hora >= 0 && hora <= 23) {
			this.hora = hora;
		}
	}

	public byte getHora() {
		return this.hora;
	}

	public void setMinuto(byte minuto) {
		if (minuto >= 0 && minuto <= 59) {
			this.minuto = minuto;
		}
	}

	public byte getMinuto() {
		return this.minuto;
	}

	public void setSegundo(byte segundo) {
		if (segundo >= 0 && segundo <= 59) {
			this.segundo = segundo;
		}
	}

	public byte getSegundo() {
		return this.segundo;
	}

	public String toString() {
		return getHora() + ":" + getMinuto() + ":" + getSegundo();
	}

	public void incrementaSegundo() {

		byte s = (byte) (segundo + 1);

		if (s == 60) {
			segundo = 0;
			incrementaMinuto();
		} else {
			segundo = s;
		}
	}

	public void incrementaMinuto() {
		byte m = (byte) (minuto + 1);

		if (m == 60) {
			minuto = 0;
			incrementaHora();
		} else {
			minuto = m;
		}
	}

	public void incrementaHora() {
		byte h = (byte) (hora + 1);

		if (h == 24) {
			hora = 0;
		} else {
			hora = h;
		}
	}

	public boolean ehUltimoHorario() {
		return hora == 23 && minuto == 59 && segundo == 59;
	}

	public boolean ehPrimeiroHorario() {
		return hora == 0 && minuto == 0 && segundo == 0;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + hora;
		result = prime * result + minuto;
		result = prime * result + segundo;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Horario other = (Horario) obj;
		if (hora != other.hora)
			return false;
		if (minuto != other.minuto)
			return false;
		if (segundo != other.segundo)
			return false;
		return true;
	}
}
