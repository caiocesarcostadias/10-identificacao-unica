package atividade;

public interface IHorario {

	void setHora(byte hora);

	byte getHora();

	void setMinuto(byte minuto);

	byte getMinuto();

	void setSegundo(byte segundo);

	byte getSegundo();

	String toString();

	void incrementaSegundo();

	void incrementaMinuto();

	void incrementaHora();

	void incrementaSegundo(int n);

	boolean ehUltimoHorario();

	boolean ehPrimeiroHorario();
	
	boolean menorOuIgual(IHorario horario);
	
	boolean maiorOuIgual(IHorario horario);
	
	boolean maior(IHorario horario);
}
